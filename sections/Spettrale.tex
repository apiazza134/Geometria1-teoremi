% TEOREMA SPETTRALE, AGGIUNZIONE, OPERATORI ORTOGONALI E UNITARI

Sia $ V $ uno spazio vettoriale sul campo $ \K = \R \text{ o } \C $ di dimensione finita dotato di prodotto rispettivamente scalare o hermitiano definito positivo $ \sp{\;}{\,} $.

\begin{thm}[endomorfismo aggiunto e matrice aggiunta]
	Dato $ T \colon V \to V $ endomorfismo esiste un unico endomorfismo $ T^{*} \colon V \to V $ tale che \[\forall u, v \in V, \ \sp{Tu}{v} = \sp{u}{T^{*}v}.\] Tale $ T^{*} $ viene detto endomorfismo aggiunto di $ T $. \\
	In termini di matrici, se $ \mathscr{B} $ è una base ortonormale di $ V $ e $ [T]_{\mathscr{B}}^{\mathscr{B}} $ è la matrice di $ T $ rispetto a tale base, allora la matrice di $ T^{*} $ rispetto alla stessa base è $ [T^{*}]_{\mathscr{B}}^{\mathscr{B}} = \overline{([T]_{\mathscr{B}}^{\mathscr{B}})^{t}} $ (trasposta coniugata). Se $ A $ è una matrice quadrata a coefficienti in $ \R $ o $ \C $, si definisce matrice aggiunta di $ A $ la matrice $ \overline{A^t} $.
\end{thm}

\begin{prop}[matrice dell'aggiunto]
	Sia $ T \in \End{(V)} $, $ \mathscr{B} $ una base di $ V $ e $ M $ la matrice del prodotto scalare scritta in tale base. Allora
	\begin{itemize}
		\item $ \K = \R $: $ [T^{*}]_{\mathscr{B}}^{\mathscr{B}} = M^{-1} \, ([T]_{\mathscr{B}}^{\mathscr{B}})^{t} \, M $.
		\item $ \K = \C $: $ [T^{*}]_{\mathscr{B}}^{\mathscr{B}} = \overline{M^{-1}} \, \overline{([T]_{\mathscr{B}}^{\mathscr{B}})^{t}} \, \overline{M} $.
	\end{itemize}
\end{prop}

\begin{prop}[proprietà dell'aggiunzione]
	Dati $ T, S \in \End{(V)} $ vale
	\begin{enumerate}[label = (\roman*)]
		\item $ (T + S)^{*} = T^{*} + S^{*} $
		\item $ (T \, S)^{*} = S^{*} \, T^{*} $
		\item $ \forall \alpha \in \K, \ (\alpha \, T)^{*} = \overline{\alpha} \, T^{*} $
		\item $ (T^{*})^{*} = T $
		\item $\dagger$ $ \ker{T^{*}} = (\im{T})^{\perp} $ e $ \ker{T} = (\im{T^{*}})^{\perp} $;
		\item $\dagger$ $ \im{T^{*}} = (\ker{T})^{\perp} $ e $ \im{T} = (\ker{T^{*}})^{\perp} $;
		\item $\dagger$ $ \det{(T^{*})} = (\det{T})^{*} $ e $ \tr{(T^{*})} = (\tr{T})^{*} $.
	\end{enumerate}
\end{prop}

\begin{definition}[endomorfismo normale]
	$ T \in \End{(V)} $ si dice normale se commuta con il suo aggiunto, ovvero se $ T \; T^{*} = T^{*} \; T $.
\end{definition}

\begin{definition}[endomorfismo autoaggiunto]
	$ T \in \End{(V)} $ si dice autoaggiunto se $ T = T^{*} $. In altri termini se $ [T]_{\mathscr{B}}^{\mathscr{B}} $ è la matrice di $ T $ rispetto a una base ortonormale $ \mathscr{B} $ di $ V $ si ha
	\begin{itemize}
		\item $ \K = \R $: $ T $ è autoaggiunta $ \iff $ $ [T]_{\mathscr{B}}^{\mathscr{B}} $ è simmetrica (uguale alla trasposta).
		\item $ \K = \C $: $ T $ è autoaggiunta $ \iff $ $ [T]_{\mathscr{B}}^{\mathscr{B}} $ è hermitiana (uguale alla trasposta coniugata).
	\end{itemize}
\end{definition}

\begin{thm}
	Sia $ T \in \End{(V)} $ autoaggiunto. Se $ \lambda $ autovalore per $ T $, allora $ \lambda \in \R $.
\end{thm}

\begin{thm} \label{thm:autoaggiunto_R}
	$ \K = \R $ e $ T \in \End{(V)} $ autoaggiunto. Allora
	\begin{enumerate}[label = (\roman*)]
		\item il polinomio caratteristico $ p_T(t) $ si fattorizza completamente e ha tutte le radici reali;
		\item $ T $ ha almeno un autovalore;
		\item se $ \{v_1, \ldots, v_r\} $ è un insieme di autovettori a due a due distinti allora $ v_1, \ldots, v_r $ sono a due a due ortogonali.
	\end{enumerate}
\end{thm}

\begin{prop} \label{thm:invarianza_ortogonale_aggiunto}
	Sia $ T \in \End{(V)} $ e $ W $ sottospazio di $ V $ $ T $-invariante, i.e. $ T(W) \subseteq W $. Allora l'ortogonale $ W^{\perp} $ è $ T^{*} $-invariante.
\end{prop}

\begin{prop} \label{thm:invarianza_sottospazio_autoaggiunto}
	Sia $ T \in \End{(V)} $ autoaggiunto e $ W $ sottospazio di $ V $ $ T $-invariante, i.e. $ T(W) \subseteq W $. Allora $ T\lvert_{W} $ è ancora autoaggiunto.
\end{prop}
\begin{proof}
	Poiché $ W $ è $ T $-invariante l'applicazione $ T\lvert_{W} \colon W \to W $ è ben definita come endomorfismo e anche $ T^{*}\lvert_{W} \colon W \to W $ è ben definita poiché $ T $ è autoaggiunto e quindi $ W $ è anche $ T^{*} $ invariante. Così $ \forall w_1, w_2 \in W $ si ha per definizione di aggiunto che $ \sp{T\lvert_{W} w_1}{w_2} = \sp{w_1}{\left(T\lvert_{W}\right)^{*} w_2} $. Tale relazione è anche soddisfatta da $ T $ ovvero $ \sp{T w_1}{w_2} = \sp{w_1}{T^{*} w_2} $. Ma poiché $ T w_1 = T\lvert_{W} w_1 $ e $ T^{*}w_2 = T^{*}\lvert_{W}w_2 $ troviamo che $ \sp{w_1}{\left(T\lvert_{W}\right)^{*} w_2} = \sp{w_1}{T^{*}\lvert_{W} w_2} $ per ogni $ w_1, w_2 \in W $. Per unicità dell'aggiunto poiché $ T $ è autoaggiunto deve essere $ \left(T\lvert_{W}\right)^{*} = T^{*}\lvert_{W} = T\lvert_{W} $, che è la tesi.
\end{proof}

\begin{thm}[Spettrale $ \K = \C $] $\dagger$
	$ T \colon V \to V $ è endomorfismo normale se e solo se esiste una base ortonormale di $ V $ di autovettori per $ T $.\\
	In altri termini, ogni matrice normale è simile a una matrice diagonale tramite una matrice unitaria. In formule se $ N \in \mathrm{Mat}_{n \times n}(\C) $ è una matrice normale esiste esistono una matrice unitaria $ U $ (i.e. $ \overline{U^t} U = \Id $) e una matrice diagonale $ D $ tali che \[D = U^{-1} N U = \overline{U^t} N U.\]
\end{thm}

\begin{thm}[Spettrale $ \K = \C $ per endomorfismi autoaggiunti] \label{thm:spettrale_C}
	Sia $ T \colon V \to V $ endomorfismo autoaggiunto. Allora esiste una base ortonormale di $ V $ di autovettori per $ T $ (una sola implicazione).\\
	In altri termini, ogni matrice hermitiana è simile a una matrice diagonale tramite una matrice unitaria. In formule se $ H \in \mathrm{Mat}_{n \times n}(\C) $ è una matrice hermitiana esiste esistono una matrice unitaria $ U $ (i.e. $ \overline{U^t} U = \Id $) e una matrice diagonale $ D $ tali che \[D = U^{-1} H U = \overline{U^t} H U.\]
\end{thm}
\begin{proof}
	Per induzione sulla $ \dim{V} = n $. Per $ n = 1 $ l'enunciato è banalmente verificato. Sia $ n > 1 $ e $ v \in V $ autovettore per $ T $ (che esiste perché il polinomio caratteristico è completamente fattorizzabile). Sia $ W = Span{\{v\}} $ e $ W^{\perp} $ il suo ortogonale. Poiché il prodotto scalare è definito positivo vale $ V = W \oplus W^{\perp} $. $ T\lvert_{W} $ è diagonalizzabile per definizione di $ W $. Per la Proposizione \ref{thm:invarianza_ortogonale_aggiunto} sappiamo che $ W^{\perp} $ è $ T^{*} $ invariante e quindi anche $ T $ invariante poiché l'endomorfismo è autoaggiunto: così $ T\lvert_{W^{\perp}} $ è autoaggiunto per la Proposizione \ref{thm:invarianza_sottospazio_autoaggiunto}. Ma allora poiché $ \dim{W^{\perp}} = n - 1 $ per ipotesi induttiva esiste una base $ \{v_2, \ldots, v_n\} $ di $ W^{\perp} $ di autovettori per $ T\lvert_{W^{\perp}} $ ortonormali. Segue che $ \left\{\frac{v}{\norm{v}}, v_2, \ldots, v_n\right\} $ è la base cercata. \\
	Per la parte sulle matrici, ogni matrice hermitiana $ H \in \mathrm{Mat}_{n \times n}(\C) $ può essere vista come matrice di un endomorfismo autoaggiunto su $ \C^{n} $, dotato del prodotto hermitiano standard. Per il Teorema Spettrale esiste una base ortonormale rispetto al prodotto hermitiano standard di autovettori per $ H $ e la corrispondente matrice di cambio base $ U $ (dalla digonalizzante alla canonica) ha come colonne i vettori della base ortonormale. Per la Proposizione \ref{thm:unitaria} questo equivale a dire che $ U $ è matrice unitaria ovvero che $ U^{-1} = \overline{U^{t}} $ che ci dà la tesi.
\end{proof}

\begin{thm}[Spettrale $ \K = \R $]
	$ T \colon V \to V $ è endomorfismo autoaggiunto se e solo se esiste una base ortonormale di $ V $ di autovettori per $ T $. \\
	In altri termini, ogni matrice simmetrica reale è simile a una matrice diagonale tramite una matrice ortogonale. In formule se $ S \in \mathrm{Mat}_{n \times n}(\R) $ è una matrice simmetrica reale esistono una matrice ortogonale $ O $ (i.e. $ O^{t} O = \Id $) e una matrice diagonale $ D $ tali che \[D = O^{-1} S O = O^{t} S O.\]
\end{thm}
\begin{proof}
	Se $ T $ è autoaggiunto procediamo per induzione come nel Teorema \ref{thm:spettrale_C}. L'esistenza di un autovettore nel passo induttivo è questa volta garantito dal Teorema \ref{thm:autoaggiunto_R}. \\
	Se invece esiste una base $ \mathscr{D} $ ortonormale di autovettori per $ T $ allora la matrice $ [T]_{\mathscr{D}}^{\mathscr{D}} $ è diagonale, quindi simmetrica in una base ortonormale e quindi $ T $ è autoaggiunto.\\
	Per la parte sulle matrici, ogni matrice simmetrica $ S \in \mathrm{Mat}_{n \times n}(\R) $ può essere vista come matrice di un endomorfismo autoaggiunto su $ \R^{n} $, dotato del prodotto scalare standard. Per il Teorema Spettrale esiste una base ortonormale rispetto al prodotto scalare standard di autovettori per $ S $ e la corrispondente matrice di cambio base $ O $ (dalla diagonalizzante alla canonica) ha come colonne i vettori della base ortonormale. Per la Proposizione \ref{thm:ortogonale} questo equivale a dire che $ O $ è matrice ortogonale ovvero che $ O^{-1} = O^{t} $ che ci dà la tesi.
\end{proof}

\begin{prop}
	Sia $ T \in \End{(V)} $ (autoaggiunto se $ \K = \R $). Allora \[T = O_{\End{V}} \iff \forall v \in V, \ \sp{Tv}{v} = 0\]
\end{prop}
\begin{proof}
	Se $ T $ è l'endomorfismo nullo allora l'implicazione è ovvia. Supponiamo ora che si abbia $ \sp{Tv}{v} = 0 $ per ogni $ v \in V $. Da ciò deduciamo che $ \sp{T(v + w)}{v + w} = 0 \Rightarrow \sp{Tv}{w} + \sp{Tw}{v} = 0 $. Distinguiamo quindi due casi:
	\begin{itemize}
		\item $ \K = \C $ - La relazione trovata resta vera se la applichiamo a $ v, i w $, così $ \sp{Tv}{iw} + \sp{T iw}{v} = 0 \Rightarrow - i \sp{Tv}{w} + i \sp{Tw}{v} = 0 \Rightarrow - \sp{Tv}{w} + \sp{Tw}{v} = 0 $. Sommata alla relazione precedente ci dà $ \sp{Tw}{v} = 0 $ per ogni $ v, w \in V $. Ma poiché il prodotto scalare è non degenere deve essere $ Tw = O_{V} $ per ogni $ w \in V $ ovvero $ T $ è l'endomorfismo nullo.
		\item $ \K = \R $ - Poiché $ T $ è autoaggiunto e il prodotto scalare è simmetrico $ \sp{Tv}{w} = \sp{v}{Tw} = \sp{Tw}{v} $. Dalla relazione precedente troviamo quindi $ \sp{Tw}{v} = 0 $ per ogni $ v, w \in V $. Ma poiché il prodotto scalare è non degenere deve essere $ Tw = O_{V} $ per ogni $ w \in V $ ovvero $ T $ è l'endomorfismo nullo. \qedhere
	\end{itemize}
\end{proof}

\begin{thm} $\dagger$
	$ \K = \R $. Sia $ T \in \End{(V)} $ tale che $ T T^{*} = T^{*} T $. Allora $ V = \ker{T} \oplus \im{T} $.
\end{thm}

\begin{definition}[endomorfismo ortogonale e matrice ortogonale]
	$ \K = \R $. Un endomorfismo $ U \colon V \to V $ tale che $ U^{*} = U^{-1} $ si dice endomorfismo ortogonale. In termini di matrici se $ \mathscr{B} $ è un base ortonormale, $ [U]_{\mathscr{B}}^{\mathscr{B}} $ è ortogonale $ \iff $ $ [U^{-1}]_{\mathscr{B}}^{\mathscr{B}} = ([U]_{\mathscr{B}}^{\mathscr{B}})^t $.\\
	Equivalentemente $ M \in \mathrm{Mat}_{n \times n}(\R) $ si dice ortogonale se $ M^{t} = M^{-1} $.
\end{definition}

\begin{prop} \label{thm:ortogonale}
	Le seguenti affermazioni sono equivalenti.
	\begin{enumerate}[label = (\roman*)]
		\item $ A \in \mathrm{Mat}_{n \times n}(\R) $ è ortogonale.
		\item Le righe di $ A $ sono vettori ortonormali rispetto al prodotto scalare standard.
		\item Le colonne di $ A $ sono vettori ortonormali rispetto al prodotto scalare standard.
	\end{enumerate}
\end{prop}

\begin{definition}[endomorfismo unitario e matrice unitaria]
	$ \K = \C $. Un endomorfismo $ U \colon V \to V $ tale che $ U^{*} = U^{-1} $ si dice endomorfismo unitario. In termini di matrici se $ \mathscr{B} $ è un base ortonormale, $ [U]_{\mathscr{B}}^{\mathscr{B}} $ è unitaria $ \iff $ $ [U^{-1}]_{\mathscr{B}}^{\mathscr{B}} = \overline{([U]_{\mathscr{B}}^{\mathscr{B}})^{t}} $\\
	Equivalentemente $ M \in \mathrm{Mat}_{n \times n}(\C) $ si dice unitaria se $ \overline{M^{t}} = M^{-1} $.
\end{definition}

\begin{prop} \label{thm:unitaria}
	Le seguenti affermazioni sono equivalenti.
	\begin{enumerate}[label = (\roman*)]
		\item $ A \in \mathrm{Mat}_{n \times n}(\C) $ è unitaria.
		\item Le righe di $ A $ sono vettori ortonormali rispetto al prodotto hermitiano standard.
		\item Le colonne di $ A $ sono vettori ortonormali rispetto al prodotto hermitiano standard.
	\end{enumerate}
\end{prop}

\begin{prop}
	Sia $ U \in \End{V} $ tale che $ U^{-1} = U^{*} $ (i.e. ortogonale o unitario), allora $ \abs{\det{U}} = 1 $. In particolare se $ U $ è operatore ortogonale vale $ \det{U} = \pm 1 $. \\
	Dunque $ U $ è invertibile ed è pertanto un isomorfismo.
\end{prop}

\begin{definition}[gruppo degli operatori ortogonali]
	L'insieme degli endomorfismi/operatori ortogonali su $ V $ con l'operazione di composizione è un gruppo e viene indicato con $ O(V) $. Il sottogruppo delle di $ O(V) $ con determinante $ + 1 $ è il gruppo ortogonale speciale indicato con $ SO(V) $. \\
	Se $ V = \R^n $ spesso si usa la notazione $ O(n) $ e $ SO(n) $.
\end{definition}

\begin{definition}[gruppo degli operatori unitari]
	L'insieme degli endomorfismi/operatori unitari su $ V $ con l'operazione di composizione è un gruppo e viene indicato con $ U(V) $. Il sottogruppo delle di $ U(V) $ con determinante $ + 1 $ è il gruppo ortogonale speciale indicato con $ SU(V) $. \\
	Se $ V = \C^n $ spesso si usa la notazione $ U(n) $ e $ SU(n) $.
\end{definition}

\begin{thm}
	Sia $ U \in \End{(V)} $ tale che $ U^{*} = U^{-1} $ (i.e. ortogonale o unitario). Se $ \lambda $ è autovalore per $ U $ allora
	\begin{enumerate}[label = (\roman*)]
		\item $ \abs{\lambda} = 1 $ (in particolare se $ \lambda \in \R $ allora $ \lambda  = \pm 1 $);
		\item Se $ U v = \lambda v $ allora $ U^{*} v = \overline{\lambda} v $ (in particolare $ \overline{\lambda} $ è autovalore di $ U^{*} = U^{-1} $ rispetto allo stesso autovettore).
	\end{enumerate}
\end{thm}

\begin{thm} \label{thm:proprietà_unitarie}
	Dato $ U \in \End{(V)} $ sono equivalenti
	\begin{enumerate}[label = (\roman*)]
		\item $ U^{*} = U^{-1} $;
		\item $ \forall v, w \in V, \ \sp{Uv}{Uw} = \sp{v}{w} $ ($ \iff d(Uv, Uw) = d(v, w) $ $ \iff $ $ U $ è un'isometria)
		\item $ \forall v \in V, \ \norm{Uv} = \norm{v} $.
	\end{enumerate}
\end{thm}

\begin{prop} \label{thm:invarianza_ortogonale_unitario}
	Sia $ U \in \End{(V)} $ ortogonale o unitaria e sia $ W $ un sottospazio di $ V $ $ U $-invariante. Allora $ W^{\perp} $ è $ U $-invariante.
\end{prop}

\begin{thm}[Spettrale per gli endomorfismi unitari]
	$ \K = \C $. Sia $ U \in \End{(V)} $ unitario. Allora esiste una base ortonormale di $ V $ di autovettori per $ U $.
\end{thm}
\begin{proof}
	Per induzione sulla $ \dim{V} = n $. Per $ n = 1 $ l'enunciato è banalmente verificato. Sia $ n > 1 $ e $ v \in V $ autovettore per $ U $ (che esiste perché il polinomio caratteristico è completamente fattorizzabile). Sia $ W = Span{\{v\}} $ e $ W^{\perp} $ il suo ortogonale, così $ V = W \oplus W^{\perp} $ dato che il prodotto scalare è definito positivo. Poiché $ W $ è $ U $-invariante per la Proposizione \ref{thm:invarianza_ortogonale_unitario} sappiamo che anche $ W^{\perp} $ è $ U $ invariante. Consideriamo quindi $ U\lvert_{W^{\perp}} \colon W^{\perp} \to W^{\perp} $. Poiché $ U $ è invertibile ($ \det{U} \neq 0 $), $ W^{\perp} $ è anche $ U^{-1} = U^{*} $ invariante. Si verifica che $ \left(U\lvert_{W^{\perp}}\right)^{*} = \left(U\lvert_{W^{\perp}}\right)^{-1} $. Così $ U\lvert_{W^{\perp}} $ è operatore unitario ed essendo $ \dim{W^{\perp}} = n - 1 $ per ipotesi induttiva esiste una base $ \{v_2, \ldots, v_n\} $ di $ W^{\perp} $ di autovettori per $ U\lvert_{W^{\perp}} $ ortonormali. Segue che $ \left\{\frac{v}{\norm{v}}, v_2, \ldots, v_n\right\} $ è la base cercata.
\end{proof}

\begin{thm}[forma canonica degli endomorfismi ortogonali]
	Sia $ Q \in O(n) $ un endomorfismo ortogonale. Allora esiste una base ortonormale $ \mathscr{B} $ di $ V $ in cui la matrice di $ Q $ ha la forma
	\[[Q]_{\mathscr{B}}^{\mathscr{B}} =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ \Id_{p} $} &  &  &  &  \\
	& \fbox{$ - \Id_{q} $} &  &  &  \\
	&  &\fbox{$ R_{\theta_1} $} &  &  \\
	&  &  & \ \ddots \ &  \\
	&  &  &  & \fbox{$ R_{\theta_k} $}
	\end{pmatrix} \]
	dove $ p, q, k \in \N $ con $ p + q + 2 k = n = \dim{V} $, $ \Id_r $ è la matrice identità di dimensioni $ r \times r $ e $ R_{\theta_i} $ è una matrice rotazione non banale $ 2 \times 2 $
	\[R_{\theta_i} =
	\begin{pmatrix}
	\cos{\theta_i} & - \sin{\theta_i} \\
	\sin{\theta_i} & \cos{\theta_i}
	\end{pmatrix}
	\quad
	\text{ con $ \theta_{i} \neq k \pi \ \forall k \in \Z $}\]
\end{thm}
\begin{proof}
	Consideriamo $ T = Q + Q^{*} \colon V \to V $ che è un endomorfismo autoaggiunto. Per il Teorema Spettrale $ T $ è diagonalizzabile, ha autovalori $ \lambda_1, \ldots, \lambda_k $ distinsi e $ V = V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} $. Se $ v \in V_{\lambda_j} $ si ha \[TQv = Q Q v + Q^{*} Q v = Q Q v + Q Q^{*} v = Q T v = \lambda_j Q v,\] dove la seconda uguaglianza è giustificata dal fatto che per definizione di operatore ortogonale $ Q^{*} = Q^{-1} $ è quindi l'operatore aggiunto commuta con l'operatore stesso. Dunque $ Qv \in V_{\lambda_j} $ ovvero i $ V_{\lambda_j} $ sono $ Q $-invarianti. Restringiamoci quindi a $ Q\lvert_{V_{\lambda_j}} $ che per semplicità indicheremo con $ Q_j $. \\
	Sia $ v \in V_{\lambda_j} $. Per costruzione sappiamo che $ T\lvert_{V_{\lambda_j}} v = Tv = \lambda_j v $ ovvero che \[Q_j v + Q_j^{*} v = \lambda_j v.\] Applicando $ Q_j $ a entrambi i membri, ricordando che $ Q_j Q_j^{*} = \Id $ otteniamo \[(Q_j^2 - \lambda_j Q_j  + \Id)v = O_{V}.\] Dunque per genericità di $ v $ abbiamo che $ Q_j^2 - \lambda_j Q_j  + \Id $ è l'operatore nullo e quindi il polinomio minimo di $ Q_j $ divide $ p_j(t) = t^{2} - \lambda_j t + 1 $. $ p_j(t) $ ha radici multiple solo se $ \lambda_j = \pm 2 $. Distinguiamo quindi in tre casi:
	\begin{itemize}
		\item Se $ \lambda_j = 2 $ allora $ Q_j^2 - 2 Q + \Id = (Q_j - \Id)^2 $ è l'endomorfismo nullo. Dunque il polinomio minimo di è o $ \mu_j(t) = t - 1 $ o $ \mu_j(t) = (t - 1)^2 $. Si ha per ogni $ v \in V_{2} $
		\begin{align*}
			\sp{(Q_j - \Id) v}{(Q_j - \Id) v} & = \sp{v}{(Q_j - \Id)^{*}(Q_j - \Id) v} \\
			& = \sp{v}{(Q_j^{*} - \Id)(Q_j - \Id) v} \\
			& = \sp{v}{(Q_j^{*} Q_j - Q_j^{*} - Q_j + \Id) v} \\
			& = \sp{v}{(2 \Id - T) v} \\
			& = \sp{v}{2v - 2v} \\
			& = \sp{v}{O_V} = 0
		\end{align*}
		che implica $ (Q_j - \Id) v = O_V $ per ogni $ v \in V_2 $ ovvero $ Q_j - \Id $ è l'endomorfismo nullo ossia $ \mu_j(t) = t - 1 $ e $ Q_j = \Id $.
		\item Se $ \lambda = -2 $ si procede in modo analogo e si trova che $ Q_j = -\Id $.
		\item Se $ \lambda_j \neq \pm 2 $ allora $ Q_j $ non è diagonalizzabile: infatti se $ Q_j $ ha autovalori essi devono essere reali di norma 1, ovvero $ \pm 1 $; ma se $ \lambda_j \neq \pm 2 $ allora $ \pm 1 $ non sono radici di $ p_j(t) $ e quindi $ Q_j $ non ha né autovalori né autovettori in $ V_{\lambda_j} $. \\
		Così se $ v \in V_{\lambda_j} $ osserviamo che $ v $ e $ Q_j v $ sono linearmente indipendenti e sia $ W = Span{\{v, Q_jv\}} $. $ W $ è $ Q_j $ invariante in quanto chiaramente $ Q_jv \in W $ e inoltre $ Q_j^2 v = \lambda_j Q_j v - v \in W $. Così $ V_{\lambda_j} $ si spezza come $ V_{\lambda_j} = W \oplus W^{\perp} $. Ragionando induttivamente su $ W^{\perp} $ spezziamo $ V_{\lambda_j} $ come somma diretta di sottospazi ortogonali bidimensionali (piani) $ Q_j $-invarianti (dunque $ V_j $ ha dimensione pari perché se avesse un sottospazio di dimensione 1 $ Q_j $-invariante, $ Q_j $ avrebbe un autovettore). \\
		Studiamo quindi $ Q_j \lvert_{W} $: rispetto ad una base ortonormale $ Q_j \lvert_{W} $ è rappresentabile da una matrice $ 2 \times 2 $
		\[\begin{pmatrix}
		\alpha & \beta \\
		\gamma & \delta
		\end{pmatrix}\]
		Imponendo la condizione di ortogonalità (righe ortonormali rispetto al prodotto calare standard e determinante uguale a 1) troviamo $ \alpha = \delta $, $ \beta = - \gamma $ e $ \alpha^2 + \beta^2 $ ovvero $ \abs{\alpha} \leq 1 $ e quindi possiamo porre $ \alpha = \cos{\theta} $ e $ \beta = \pm \sin{\theta} $. Così a meno di scegliere $ 2\pi - \theta $ al posto di $ \theta $ la matrice di $ Q_j \lvert_{W} $ si può scrivere in un'opportuna base come
		\[R_{\theta} =
		\begin{pmatrix}
		\cos{\theta} & - \sin{\theta} \\
		\sin{\theta} & \cos{\theta}
		\end{pmatrix}\]
		che è una rotazione di angolo $ \theta \neq 0, 2\pi $
	\end{itemize}
	Mettendo insieme quanto detto otteniamo la tesi.
\end{proof}
% DIAGONALIZZAZIONE DI ENDOMORFISMI

\begin{definition}[autovettore, autovalore]
	Sia $ V $ un $ \K $-spazio vettoriale. Sia $ T \colon V \to V $ un endomorfismo su $ V $. Un vettore $ v \in V \setminus \{O_{V}\} $ si dice autovettore di $ T $ se esiste $ \lambda \in \K $ tale che \[T(v) = \lambda v.\] Si dice in questo caso che $ \lambda $ è autovalore per $ T $ (relativo a $ v $).
\end{definition}

\begin{definition}[autospazio]
	Dato $ \lambda \in \K $ chiamiamo $ V_\lambda = \{v \in V : T(v) = \lambda v\} $ l'autospazio relativo a $ \lambda $. Segue dalla definizione che $ V_\lambda = \ker{\left(T - \lambda \cdot \Id\right) }$.\\
	Notiamo che tutti i $ v \in \ker{V} \setminus \{O_{V}\} $ sono autovettori per $ v $ con autovalore 0 e quindi che $ V_0 = \ker{T} $.
\end{definition}

\begin{prop}
	Se $ v_1, \ldots, v_n \in V $ sono autovettori per $ T $ con relativi autovalori $ \lambda_1, \ldots, \lambda_n \in \K $ e $ \mathscr{B} = \{v_1, \ldots, v_n\} $ è una base di $ V $ allora la matrice di $ T \in \End{(V)} $ rispetto a $ \mathscr{B} $ (sia in partenza che in arrivo) è diagonale.
	\[[T]_{\mathscr{B}}^{\mathscr{B}} =
	\begin{pmatrix}
	\lambda_1 & \cdots & 0 \\
	\vdots & \ddots & \vdots \\
	0 & \ldots & \lambda_n \\
	\end{pmatrix}\]
\end{prop}

\begin{definition}[polinomio caratteristico]
	Sia $ T \in \End{(V)} $. Fissata una base $ \mathscr{B} = \{v_1, \ldots, v_n\} $ di $ V $ chiamiamo \[p_T(t) = \det \left(t \cdot \Id - [T]_{\mathscr{B}}^{\mathscr{B}}\right)\] il polinomio caratteristico di $ T $.
\end{definition}

\begin{prop}
	Il polinomio caratteristico non dipende dalla base scelta. In altri termini se $ \mathscr{B} = \{v_1, \ldots, v_n\} $ e $ \mathscr{B}' = \{v'_1, \ldots, v'_n\} $ sono basi di $ V $ allora \[p_T(t) = \det \left(t \cdot \Id - [T]_{\mathscr{B}}^{\mathscr{B}}\right) = \det \left(t \cdot \Id - [T]_{\mathscr{B}'}^{\mathscr{B}'}\right) = \det \left(t \cdot \Id - T\right)\]
\end{prop}

\begin{thm}
	Sia $ T \in \End{(V)} $. Allora $ \lambda \in \K $ è un autovalore di $ T $ se e solo se $ \lambda $ è radice di $ p_T(t) $, ossia $ p_T(\lambda) = 0 $.
\end{thm}

\begin{thm}
	Dati $ \lambda_1, \ldots, \lambda_k $ autovalori di $ T \in \End{(V)} $ a due a due distinti, siano $ v_1, \ldots, v_k $ gli autovettori corrispondenti: $ T(v_1) = \lambda_1 v_1, \ldots, T(v_k) = \lambda_k v_k $. Allora $ v_1, \ldots, v_k $ sono linearmente indipendenti.
\end{thm}
\begin{proof}
	Per induzione su $ k $. Se $ k = 1 $ allora l'enunciato è ovvio. Supponiamo si vero per un numero di autovettori $ \leq k - 1 $. Se $ a_1, \ldots, a_k \in \K $ tali che \[a_1 v_1 + \ldots + a_k v_k = O_{V}\] allora applicando $ T $ a entrambi i membri otteniamo \[a_1 \lambda_1 v_1 + \ldots + a_k \lambda_k v_k = O_{V}.\] Così sottendo la prima equazione moltiplicata per $ \lambda_1 $ alla seconda otteniamo \[a_2 (\lambda_2 - \lambda_1) v_2 + \ldots + a_k(\lambda_k - \lambda_1)v_k = O_{V}.\] Per ipotesi induttiva $ v_2, \ldots, v_k $ sono linearmente indipendenti e quindi $ a_2(\lambda_2 - \lambda_1) = \ldots = a_k(\lambda_k - \lambda_1) = 0 \Rightarrow a_2 = \ldots = a_k = 0 $ poiché gli autovalori sono a due a due distinti. Riprendendo la prima equazione otteniamo infine $ a_1 = 0 $ che è quanto volevamo.
\end{proof}

\begin{thm}[somma diretta degli autospazi]
	Sia $ T \in \End{(V)} $ e $ \lambda_1, \ldots, \lambda_k $ autovalori di $ T $ a due a due distinti. Allora gli autospazi $ V_{\lambda_1}, \ldots, V_{\lambda_k} $ sono in somma diretta. \\
	Più in generale se $ A_1, \ldots, A_k $ sottospazi di $ V $ sono tali che per ogni insieme $ \{v_1, \ldots, v_k\} $ di vettori non nulli linearmente indipendenti tali che $ v_i \in A_i $, allora $ A_1, \ldots, A_k $ sono in somma diretta.
\end{thm}

\begin{definition}[molteplicità algebrica e geometrica]
	Sia $ T \in \End{(V)} $ e \[p_T(t) = (t - \lambda_1)^{a_1} \cdots (t - \lambda_k)^{a_k} \cdot f(t)\] il polinomio caratteristico dove $ \lambda_1, \ldots, \lambda_k $ sono le radici del polinomio, i.e autovalori di $ T $, e $ f(t) $ un polinomio irriducibile in $ \K[t] $. Diremo che $ a_i $ è la molteplicità algebrica dell'autovalore $ \lambda_i $. Diremo inoltre che $ m_i = \dim{V_{\lambda_i}} = \dim{\ker{(\lambda_i \Id - T)}} $ è la molteplicità geometrica di $ \lambda_i $. \\
	Notiamo che se $ T $ è diagonalizzabile allora $ f(t) = 1 $.
\end{definition}

\begin{thm}
	Sia $ T \in \End{(V)} $ e $ \lambda_1, \ldots, \lambda_k $ con $ k \leq n $ autovalori. Allora $ \forall i = 1, \ldots, k $ vale $ 1 \leq m_i \leq a_i $ (molteplicità geometrica $ \leq $ molteplicità algebrica).
\end{thm}

\begin{thm}
	Sia $ T \in \End{(V)} $. Allora $ T $ è diagonalizzabile se e solo se $ f(t) = 1 $ (il polinomio si fattorizza completamente nel campo) e $ \forall \lambda_i $ autovalore $ m_i = a_i $.
\end{thm}
\begin{proof}
	Se $ T $ è diagonalizzabile allora, calcolando il polinomio minimo nella base digonalizzante otteniamo $ p_T(t) = (t - \lambda_1)^{m_1} \cdots (t - \lambda_k)^{m_k} $.\\
	Supponiamo ora che $ f(t) = 1 $ e che valga $ a_i = \dim{V_{\lambda_i}} $ per ogni $ \lambda_i $ e consideriamo $ V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} \subseteq V $. Allora $ p_T(t) = (t - \lambda_1)^{a_i} \cdots (t - \lambda_k)^{a_k} $ da cui otteniamo che $ a_1 + \ldots + a_k = n = \dim{V} $. Ma allora per la formula di Grassmann
	\[
		\dim{(V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k})} = \dim{V_{\lambda_1}} + \cdots + \dim{V_{\lambda_k}} = a_1 + \ldots + a_k = \dim{V}.
	\]
	Concludiamo quindi che $ V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} = V $ e dunque $ T $ è diagonalizzabile.
\end{proof}

\begin{corollary}[criterio sufficiente per la diagonalizzazione]
	Sia $ T \colon V \to V $ un endomorfismo e $ p_T(t) $ il suo polinomio caratteristico. Se $ p_T(t) $ ha tutte le radici in $ \K $ a due a due distinte allora $ T $ è diagonalizzabile.
\end{corollary}

\begin{definition}[bandiera]
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $. Si chiama bandiera per $ V $ una famiglia di sottospazi vettoriali $ V_1, \ldots, V_n $ di $ V $ tali che
	\begin{itemize}
		\item $ V_1 \subset V_2 \subset \cdots \subset V_n $;
		\item $ \forall i = 1, \ldots, n, \ \dim{V_i} = i $;
	\end{itemize}
	Osserviamo che ogni base $ \{v_1, \ldots, v_n\} $ di $ V $ induce una bandiera per $ V $ data da $ V_i = Span{\{v_1, \ldots, v_i\}} $ e viceversa ogni bandiera per $ V $ induce una base di $ V $ in quanto basta prendere induttivamente $ v \in V_i $ ma $ \notin V_{i + 1} $.
\end{definition}

\begin{definition}[base a bandiera]
	Sia $ T \in \End{(V)} $. Una base $ \mathscr{B} = \{v_1, \ldots, v_n\} $ di $ V $ si dice a bandiera per $ T $ se i sottospazi della bandiera indotta da $ \mathscr{B} $ sono $ T $-invarianti, cioè \[ \forall i \in \{1, \ldots, n\}, \ T(Span{\{v_1, \ldots, v_i\}}) \subseteq Span{\{v_1, \ldots, v_i\}}. \]
\end{definition}

\begin{definition}
	$ T \in \End{(V)} $ si dice triangolabile se esiste una base $ \mathscr{B} $ di $ V $ tale che $ [T]_{\mathscr{B}}^{\mathscr{B}} $ è triangolare superiore o inferiore.
\end{definition}

\begin{prop}
	$ T \in \End{(V)} $ è triangolabile se e solo se esiste una base a bandiera per $ T $.
\end{prop}

\begin{thm}[triangolazione]
	$ T \in \End{(V)} $ è triangolabile se e solo se $ p_T(t) $ è completamente fattorizzabile.
\end{thm}
\begin{proof}
	Se $ T $ è triangolabile esiste una base $ \mathscr{B} $ di $ V $ tale che \[[T]_{\mathscr{B}}^{\mathscr{B}} =
	\begin{pmatrix}
	\lambda_1 & & *\\
	& \ddots & \\
	0 & & \lambda_n
	\end{pmatrix}\]
	così $ p_T(t) = \det{(t \Id - [T]_{\mathscr{B}}^{\mathscr{B}})} = (t - \lambda_1) \cdots (t - \lambda_n) $ che è la tesi. \\
	Supponiamo ora che $ p_T(t) $ si fattorizzi completamente e procediamo per induzione su $ \dim{V} = n $. Per $ n = 1 $ ogni endomorfismo è triangolabile. Per $ n = 2 $ è sufficiente che esista almeno un autovettore: $ p_T(t) $ si fattorizza in $ \K $ e quindi ha almeno una radice e ogni radice del polinomio caratteristico è autovalore; quindi $ T $ è triangolabile. Sia ora $ v_1 \in V $ autovettore per $ T $ (che esiste per quanto detto in precedenza) e completiamola a una base $ \{v_1, \ldots, v_n\} $  di $ V $. Sia $ V_1 = Span{\{v_1\}} $ e $ W = Span{\{v_2, \ldots, v_n\}} $ scriviamo $ V = V_1 \oplus W $. Consideriamo le proiezioni $ \pi_{V_1} \colon V \to V_1 $ e $ \pi_W \colon V \to W $ indotte dalla somma diretta. Osserviamo ora che $ p_T(t) = (t - \lambda_1) q(t) $ è completamente fattorizzabile e quindi anche $ q(t) $ lo è e $ q $ è il polinomio caratteristico di $ \pi_{W} \circ T\lvert_{W} $; inoltre $ (\pi_W \circ T\lvert_{W}) \in \End{(W)} $ e quindi per ipotesi induttiva esiste $ \{w_1, \ldots, w_n\} $ base di $ W $ a bandiera per $ \pi_W \circ T\lvert_{W} $. Ovviamente $ \{v_1, w_2, \ldots, w_n\} $ è base di $ V $ ma è anche a bandiera: infatti $ T(v_1) = \lambda_1 v_1 $ poiché autovettore e
	\[
		T(w_i) = \left((\pi_{V_1} + \pi_W) \circ T\right) (w_i) = \underbrace{(\pi_{V_1} \circ T)(w_i)}_{\in Span{\{v_1\}}} + \underbrace{(\pi_{W} \circ T)(w_i)}_{\in Span{\{w_2, \ldots, w_n\}}}
	\]
	dove abbiamo usato il fatto che $ \pi_{V_1} + \pi_W = \Id $ e che $ (\pi_{W} \circ T)(w_i) \in Span{\{w_2, \ldots, w_n\}} $ perché per ipotesi induttiva $ \{w_2, \ldots, w_n\} $ è base a bandiera per $ \pi_W \circ T\lvert_{W} $, così $ T(w_i) \in Span{\{v_1, w_2, \ldots, w_n\}} $ da cui la tesi.
\end{proof}

\begin{corollary}
	Sia $ V $ un $ \K $-spazio vettoriale con $ \K $ algebricamente chiuso. Allora ogni $ T \in \End{(V)} $ è triangolabile. \\
	In termini di matrici, per ogni $ M \in \mathrm{Mat}_{n \times n} (\K) $ con $ \K $ algebricamente chiuso (per esempio $ \K = \C $) esiste una matrice $ C $ invertibile di tale che $ CMC^{-1} $ è triangolare superiore o inferiore (\emph{moralmente ogni matrice è triangolabile}).
\end{corollary}

\begin{definition}[polinomio minimo]
	Sia $ T \in \End{(V)} $. Chiamiamo polinomio minimo di $ T $ il polinomio di grado più piccolo (\emph{wlog} monico) $ \mu_T(t) \in \K[t] $ tale che $ \mu_T(T) $ è l'endomorfismo nullo: \[\mu_T(T) = T^j + \ldots + b_1 T + b_0 \Id = 0.\]
\end{definition}

\begin{thm}
	Sia $ T \in \End{(V)} $. Se $ h(t) \in \K[t] $ soddisfa la proprietà $ h(T) = 0 $ allora $ \mu_T(t) $ divide $ h(t) $.
\end{thm}

\begin{prop}
	Sia $ A, B \in \mathrm{Mat}_{n \times n} (\K) $ con $ B $ invertibile. Allora se $ q(t) = c_n t^{n} + \ldots + c_1 t + c_0 $ è un polinomio in $ \K[t] $ vale \[q(B^{-1} A B) = B^{-1} q(A) B. \] Se $ T \in \End{(V)} $ e $ q(t) = p_T(t) $ è il polinomio caratteristico di $ T $ (o un qualsiasi polinomio tale che $ q(T) = 0 $) si ha che $ p_T(T) = 0 \iff p_T(B^{-1} T B) = 0 $.
\end{prop}

\begin{thm}[di Hamilton - Cayley] \label{thm:HC}
	Dato $ T \colon V \to V $ endomorfismo vale che $ p_T(T) = 0 $, ovvero il polinomio minimo divide il polinomio caratteristico.
\end{thm}
\begin{proof}
	\emph{(dell'algebrista)} Per ogni matrice $ B \in \mathrm{Mat}_{n \times n}(\K) $ vale che $ B \cof(B)^{t} = \det{(B)} \Id $ dove $ \cof(B) $ è la matrice dei cofattori. Scelgo $ B = t \Id - T $ e ottengo \[(t \Id - T) \cof(t \Id - T)^{t} = \det{(t \Id - T)} \Id.\] Osserviamo che la matrice dei cofattori è fatta dai determinati dei minori di $ t \Id - T $ ovvero da polinomio in $ t $ che possono essere raccolti per grado come somma di matrici $ Q_j t^{j} $. Così se $ p_T(t) = t^{n} + c_{n - 1} t^{n - 1} + \ldots + c_1 t + c_0 $ possiamo riscrivere la relazione precedente come \[(t \Id - T) (Q_0 + Q_1 t + \ldots + Q_k t^{k}) = c_0 \Id + c_1 \Id t + \ldots + c_{n - 1} \Id t^{n - 1} + \Id t^{n}\] che sviluppando il prodotto diventa \[- T Q_0 + (Q_0 - T Q_1) t + \ldots + (Q_{k - 1} - T Q_k) t^{k} + Q_k t^{k + 1} = c_0 \Id + c_1 \Id t + \ldots + c_{n - 1} \Id t^{n - 1} + \Id t^{n}.\] Confrontando i coefficienti delle potenze di $ t $ osserviamo che deve essere necessariamente $ k + 1 = n $ (altrimenti se fosse $ k + 1 < n $ si avrebbe $ \Id = 0 $). Confrontando termine a termine otteniamo quindi le seguenti uguaglianze
	\begin{align*}
		- T Q_0 & = c_0 \Id \\
		Q_0 - T Q_1 & = c_1 \Id \\
		Q_1 - T Q_2 & = c_2 \Id \\
		& \vdotswithin{=} \\
		Q_{n - 2} - T Q_{n - 1} & = c_{n - 1} \Id \\
		Q_{n - 1} & = \Id
	\end{align*}
	Così moltiplicando la $ i $-esima equazione per $ T^{i} $
	\begin{align*}
		- T Q_0 & = c_0 \Id \\
		T (Q_0 - T Q_1) & = T c_1 \Id \\
		T^2 (Q_1 - T Q_2) & = T^2 c_2 \Id \\
		& \vdotswithin{=} \\
		T^{n - 1} Q_{n - 2} - T Q_{n - 1} & = T^{n - 1} c_{n - 1} \Id \\
		T^{n} Q_{n - 1} & = T^{n} \Id
	\end{align*}
	e sommando otteniamo la tesi \[0 = c_0 \Id + c_1 T + c_2 T^2 + \ldots + c_{n - 1} T^{n - 1} + T^{n} \quad \Rightarrow \quad p_T(T) = 0. \qedhere\]
\end{proof}
\begin{proof}
	\emph{(del geometra)} Possiamo assumere senza perdita di generalità che il campo in questione sia algebricamente chiuso e procedere in una base triangolare per $ T $. In tale base la matrice dell'endomorfismo è
	\[[T]_{\mathscr{B}}^{\mathscr{B}} = A =
	\begin{pmatrix}
	\lambda_1 & & *\\
	& \ddots & \\
	0 & & \lambda_k
	\end{pmatrix}\]
	ed il polinomio caratteristico si fattorizza come $ p_A(t) = (t - \lambda_1) \cdots (t - \lambda_k) $ e procediamo per induzione su $ k $. \\
	Per $ k = 1 $ è ovvio. Per $ k = 2 $ se
	\[A = \begin{pmatrix}
	\lambda_1 & * \\
	0 & \lambda_2
	\end{pmatrix}\]
	per un $ v \in V $ si ha
	\begin{align*}
		p_A(A) (v) & = (A - \lambda_1 \Id) (A - \lambda_2 \Id) (v) = \\
		& = \begin{pmatrix} 0 & * \\ 0 & \lambda_2 - \lambda_1 \end{pmatrix} \begin{pmatrix} \lambda_1 - \lambda_2 & * \\ 0 & 0 \end{pmatrix} \begin{pmatrix} * \\ * \end{pmatrix} \\
		& = \begin{pmatrix} 0 & * \\ 0 & \lambda_2 - \lambda_1 \end{pmatrix} \begin{pmatrix} * \\ 0 \end{pmatrix} = \begin{pmatrix} 0 \\ 0 \end{pmatrix}
	\end{align*}
	ovvero $ p_A(A) $ è l'endomorfismo nullo. \\
	Sia quindi $ \{v_1, \ldots, v_k\} $ la base a bandiera per $ T $ e $ V_1 = Span{\{v_1\}}, \ldots, V_k = Span{\{v_1, \ldots, v_n\}} $ la bandiera indotta da $ \{v_1, \ldots, v_n\} $. Preso allora $ v \in V_k $, applicando $ A - \lambda_k \Id $ a $ v $ otteniamo
	\[ (A - \lambda_k \Id) (v) =
	\begin{pmatrix}
	\lambda_1 - \lambda_k & & & * &  \\
	& \lambda_2 - \lambda_k & & & \\
	& & \ddots &  \\
	& & & \lambda_{k - 1} - \lambda_k & \\
	0 & & & & 0
	\end{pmatrix}
	\begin{pmatrix}
	* \\
	* \\
	\vdots \\
	* \\
	*
	\end{pmatrix}
	=
	\begin{pmatrix}
	* \\
	* \\
	\vdots \\
	* \\
	0
	\end{pmatrix}
	\in V_{k - 1} \]
	Sia ora $ v \in V_{k - 1} $ allora applicando $ A - \lambda_{k - 1} \Id $ a $ v $ otteniamo
	\[ (A - \lambda_{k - 1} \Id) (v) =
	\begin{pmatrix}
	\lambda_1 - \lambda_{k - 1} & & & & *  \\
	& \lambda_2 - \lambda_{k - 1} & & & \\
	& & \ddots &  \\
	& & & 0 & \\
	0 & & & & \lambda_{k} - \lambda_{k - 1}
	\end{pmatrix}
	\begin{pmatrix}
	* \\
	* \\
	\vdots \\
	* \\
	0
	\end{pmatrix}
	=
	\begin{pmatrix}
	* \\
	* \\
	\vdots \\
	0 \\
	0
	\end{pmatrix}
	\in V_{k - 2}\]
	Per induzione quindi $ \forall v \in V_k = V $ si ha $ (A - \lambda_2) \cdots (A - \lambda_k)(v) \in V_1 $ e quando applico $ A - \lambda_1 \Id $ ottengo $ O_V $. Dunque $ p_A(A) $ è l'endomorfismo nullo come volevamo.
\end{proof}

\begin{prop}
	$ T \in \End{(V)} $ è diagonalizzabile se e solo se il polinomio minimo $ \mu_T(t) $ si spezza in fattori lineari, ovvero tutte le radici hanno molteplicità algebrica 1.
\end{prop}
\begin{proof}
	Se $ T $ è diagonalizzabile, esiste una base $ \mathscr{D} $ in cui la matrice di $ T $ si scrive come
	\[[T]_{\mathscr{D}}^{\mathscr{D}} =
	\setlength{\arraycolsep}{0pt}
	\begin{pmatrix}
	\fbox{$ \lambda_1 \Id_{a_1} $} &  & \\
	& \ \ddots \ & \\
	& & \fbox{$ \lambda_k \Id_{a_k} $}
	\end{pmatrix}\]
	dove $ a_i $ è la molteplicità algebrica dell'autovalore $ \lambda_i $. Allora il polinomio $ m(t) = (t - \lambda_1) \cdots (t - \lambda_k) $ è tale che $ m(T) = (T - \lambda_1 \Id) \cdots (T - \lambda_k \Id) = 0 $ perché prodotto di matrici diagonali tali che per ogni casella della diagonale c'è uno dei fattori che è tutto nullo. Quindi il polinomio minimo divide $ m(t) $ e quindi ha radici semplici. \\
	Supponiamo ora che il polinomio minimo si spezzi in fattori lineari $ \mu_T(t) = (t - \lambda_1) \cdots (t - \lambda_k) $ con i $ \lambda_i $ tutti distinti. Gli autospazi relativi $ V_{\lambda_1} = \ker{(T - \lambda_1 \Id)}, \ldots, V_{\lambda_k} = \ker{(T - \lambda_k \Id)} $ sono in somma diretta e quindi vale
	\begin{align*}
		\dim{(V_1 \oplus \cdots \oplus V_{k})} & = \dim{V_1} + \ldots + \dim{V_k} \\
		& = \dim{\ker{(T - \lambda_1 \Id)}} + \ldots + \dim{\ker{(T - \lambda_k \Id)}} \\
		& \geq \dim{\ker{[(T - \lambda_1 \Id) \cdots (T - \lambda_1 \Id)]}} \\
		& = \dim{V}
	\end{align*}
	dove la disuguaglianza è conseguenza dalla Proposizione 4.11 (iterata) e l'ultima uguaglianza è giustificata dal fatto che per definizione di polinomio minimo $ \mu_T(T) = (T - \lambda_1 \Id) \cdots (T - \lambda_k \Id) $ è l'endomorfismo nullo e quindi per il teorema delle dimensioni vale $ \dim{V} = \dim{\ker{\mu_T(T)}} + \dim{\im{\mu_T(T)}} = \dim{\ker{\mu_T(T)}} $. Dunque $ \dim{(V_1 \oplus \cdots \oplus V_{k})} = \dim{V} $, ovvero $ V = V_1 \oplus \cdots \oplus V_{k} $ e quindi $ T $ risulta diagonalizzabile.
\end{proof}

\begin{prop}
	Sia $ T \in \End{(V)} $, $ p_T(t) $ il polinomio caratteristico e $ \mu_T(t) $ il polinomio minimo. Allora $ p_T(\lambda) = 0 \iff \mu_T(\lambda) = 0 $.
\end{prop}

\begin{thm} \label{thm:diag_sottospazio_invariante}
	Sia $ V $ un $ \K $-spazio vettoriale, $ f \in \End{(V)} $ diagonalizzabile e $ W $ un sottospazio di $ V $ $ f $-invariante. Allora \[W = (W \cap V_{\lambda_1}) \oplus \cdots \oplus (W \cap V_{\lambda_k})\] dove $ V_{\lambda_1}, \ldots, V_{\lambda_k} $ sono gli autospazi relativi agli autovalori $ \lambda_1, \ldots, \lambda_k $ di $ f $. Dunque la restrizione di $ f $ a $ W \cap V_{\lambda_j} $ è diagonalizzabile per ogni $ j $ e quindi $ f\lvert_{W} $ è diagonalizzabile.
\end{thm}
\begin{proof}
	Dato $ w \in W $, poiché $ V = V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} $, posso scriverlo in modo unico come $ w = v_1 + \ldots + v_k $ con $ v_i \in V_{\lambda_i} $. Siccome $ f(v_i) = \lambda_i v_i $ otteniamo
	\begin{align*}
		w & = v_1 + \ldots + v_k \\
		f(w) & = \lambda_1 v_1 + \ldots + \lambda_k v_k \\
		& \vdotswithin{=} \\
		f^{k - 1}(w) & = \lambda_1^{k - 1} v_1 + \ldots + \lambda_k^{k - 1} v_k
	\end{align*}
	che possiamo scrivere in forma matriciale come
	\[
		\begin{pmatrix}
			1 & \cdots & 1 \\
			\lambda_1 & \cdots & 1 \\
			\vdots & \ddots & \vdots \\
			\lambda_1^{k - 1} & \cdots & \lambda_k^{k - 1}
		\end{pmatrix}
			\begin{pmatrix}
			v_1 \\
			v_2 \\
			\vdots \\
			v_k
		\end{pmatrix}
		=
		\begin{pmatrix}
			w \\
			f(w) \\
			\vdots \\
			f^{k - 1}(w)
		\end{pmatrix}
	\]
	in cui riconosciamo una matrice di Vandermonde con determinante non nullo (i $ \lambda_i $ sono distinti per definizione) e quindi invertibile. Così ogni $ v_i $ può essere scritto come combinazione lineare di $ w, f(w), f^2(w), \ldots, f^{k - 1}(w) $. Ma poiché $ W $ è $ f $-invariante ogni combinazione lineare di $ w, f(w), f^2(w), \ldots, f^{k - 1}(w) $ che appartengono ancora a $ W $ e concludiamo quindi che $ v_1, \ldots, v_k \in W $. \\
	Dunque se $ w \in W $ possiamo scriverlo come $ w = v_1 + \ldots + v_k $ dove ogni $ v_i \in V_{\lambda_i} $. Grazie a quanto mostrato sappiamo che $ v_i \in W $ e quindi $ v_i \in W \cap V_{\lambda_i} $ e inoltre i sottospazi $ W \cap V_{\lambda_i} $ sono in somma diretta perché i $ V_{\lambda_i} $ sono in somma diretta. \\
	\textsf{Nota: per come scritta questa dimostrazione nasconde alcuni dettagli delicati, principalmente la scrittura del sistema in forma matriciale in quanto i due vettori colonna hanno come elementi vettori. La questione si dovrebbe poter risolvere interpretando la matrice come se ogni elemento fosse una piccola matrice quadrata multiplo dell'identità.}
\end{proof}

\begin{thm}[esistenza del complementare invariante] \label{thm:complementare_invariante}
	Sia $ V $ un $ \K $-spazio vettoriale, $ f \in \End{(V)} $ diagonalizzabile e $ W $ un sottospazio di $ V $ $ f $-invariante. Allora esiste $ U $ sottospazio vettoriale $ f $-invariante tale che $ V = W \oplus U $.
\end{thm}
\begin{proof}
	Per il Teorema \ref{thm:diag_sottospazio_invariante} sappiamo che $ W = (W \cap V_{\lambda_1}) \oplus \cdots \oplus (W \cap V_{\lambda_k}) $. Per ogni $ \lambda_i $ sia quindi $ U_{\lambda_i} $ il complementare di $ W \cap V_{\lambda_i} $ in $ V_{\lambda_i} $ e mostriamo che il sottospazio cercato è $ U = U_{\lambda_1} \oplus \cdots \oplus U_{\lambda_k} $. In effetti \[W \oplus U = \bigoplus_{i} (W \cap V_{\lambda_i}) \oplus \bigoplus_{i} U_{\lambda_i} = \bigoplus_{i} ((W \cap V_{\lambda_i}) \oplus U_{\lambda_i}) = \bigoplus_{i} V_{\lambda_i} = V\] e inoltre se $ u = u_{i} + \ldots + u_{k} \in \bigoplus_i U_{\lambda_i} $ allora $ f(u) = \lambda_1 u_1 + \ldots + \lambda_k u_k $ appartiene a $ \bigoplus_i U_{\lambda_i} $ perché $ u_i \in U_{\lambda_i} \Rightarrow \lambda_i u_i \in U_{\lambda_i} $.
\end{proof}

\begin{thm}
	Sia $ V $ un $ \K $-spazio vettoriale e $ f \in \End{(V)} $. Se esistono $ W_1, W_2 $ un sottospazi di $ V $ $ f $-invarianti tali che $ V = W_1 + W_2 $ e tali che le restrizioni di $ f $ a $ W_1 $ e $ W_2 $ sono diagonalizzabili, allora $ f $ è diagonalizzabile.
\end{thm}
\begin{proof}
	Si ha $ f(W_1 \cap W_2) \subseteq f(W_1) \subseteq W_1 $ e similmente $ f(W_1 \cap W_2) \subseteq W_2 $ da cui $ f(W_1 \cap W_2) \subseteq W_1 \cap W_2 $ ovvero $ W_1 \cap W_2 $ è $ f $-invariante. Per il Teorema \ref{thm:complementare_invariante} esistono $ Z_1, Z_2 $ sottospazi di $ W_1 $ e $ W_2 $ $ f $-invarianti tali che $ W_1 = (W_1 \cap W_2) \oplus Z_1 $ e $ W_2 = (W_1 \cap W_2) \oplus Z_2 $. Inltre per il Teorema \ref{thm:diag_sottospazio_invariante} sappiamo che la restrizione di $ f $ a $ W_1 \cap W_2 $, a $ Z_1 $ e a $ Z_2 $ è diagonalizzabile e inoltre $ V = Z_1 \oplus (W_1 \cap W_2) \oplus Z_2 $. Così se $ \mathscr{B}_1 $, $ \mathscr{B}_2 $ e $ \mathscr{B} $ sono basi di $ Z_1 $, $ Z_2 $ e $ W_1 \cap W_2 $ di autovettori per $ f $ è chiaro che $ \mathscr{B}_1 \cup \mathscr{B}_2 \cup \mathscr{B} $ è base di $ V $ di autovettori per $ f $ e quindi $ f $ è diagonalizzabile come voluto.
\end{proof}

\begin{thm} $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale e $ W, U $ due sottospazi di $ V $ tali che $ V = W \oplus U $. Se $ f \colon W \to W $ e $ g \colon U \to U $ sono due applicazioni lineari si consideri $ L \colon V \to V $ data da $ L(v) = f(w) + g(u) $, dove $ v = w + u $, $ w \in W $, $ u \in U $. Allora $ L $ è diagonalizzabile se e solo se $ f $ e $ g $ sono diagonalizzabili.
\end{thm}

\begin{prop}
	Siano $ T, S \in \End{(V)} $ tali che $ TS = ST $. Allora $ S $ preserva gli autospazi di $ T $ e viceversa: se $ V_{\lambda} $ è autospazio di $ T $ allora $ S(V_{\lambda}) \subseteq V_{\lambda} $ e se $ U_{\lambda} $ è autospazio di $ S $ allora $ T(U_{\lambda}) \subseteq U_{\lambda} $.
\end{prop}

\begin{thm}[diagonalizzazione simultanea]
	Siano $ T, S \in \End{(V)} $ diagonalizzabili. Allora $ T $ e $ S $ sono simultaneamente diagonalizzabili (i.e. esiste una base che diagonalizza entrambe) se e solo se $ TS = ST $.
\end{thm}
\begin{proof}
    \begin{itemize}
        \item[$ (\Leftarrow) $] $ T $ diagonalizzabile $ \Rightarrow $ $ V = V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} $. $ TS = ST $ $ \Rightarrow $ $ \forall i \in \{1, \ldots, k\}, \ S(V_{\lambda_i}) \subseteq V_{\lambda_i} $. Allora poiché $ S $ è diagonalizzabile, per il Teorema \ref{thm:diag_sottospazio_invariante} $ S\lvert_{V_{\lambda_i}} $ è diagonalizzabile. Così se $ \mathscr{B}_{\lambda_i} $ è una base di autovettori per $ S\lvert_{V_{\lambda_i}} $, $ \mathscr{B} = \mathscr{B}_{\lambda_1} \cup \cdots \cup \mathscr{B}_{\lambda_k} $ è base di $ V $ (perché $ V = V_{\lambda_1} \oplus \cdots \oplus V_{\lambda_k} $) di autovettori per $ S $ e $ T $ (se $ v \in \mathscr{B}_{\lambda_i} $ è sicuramente autovettore per $ T $ ma è anche autovettore per $ S\lvert_{V_{\lambda_i}} $ e dunque anche per $ S $).
        \item[$ (\Rightarrow) $] Posto $ D_T = M^{-1} T M $ e $ D_S = M^{-1} S M $ abbiamo $ TS = M D_T D_S M^{-1} = M D_S D_T M^{-1} = ST $
    \end{itemize}
\end{proof}

\vspace{2mm}

\begin{framed}
	\begin{center}
		\textbf{Diagonalizzazione - una strategia in 4 passi}
	\end{center}
	\begin{enumerate}
		\item Dato $ T \colon V \to V $ endomorfismo, calcolo il polinomio caratteristico e ne trovo le radici ottenendo un polinomio della forma \[p_T(t) = \det{(t \Id - T)} = (t - \lambda_1)^{a_1} \cdots (t - \lambda_k)^{a_k} \cdot f(t)\] con $ f(t) $ irriducibile in $ \K[t] $. Se il polinomio caratteristico si fattorizza completamente nel campo, i.e. $ f(t) = 1 $ allora posso procedere, altrimenti $ T $ non è diagonalizzabile.
		\item Dette $ \lambda_1, \ldots, \lambda_k $ con $ k \leq n $ le radici del polinomio caratteristico, i.e. autovalori di $ T $, studio gli autospazi relativi $ V_{\lambda_1} = \ker{(T - \lambda_1 \Id)}, \ldots, V_{\lambda_k} = \ker{(T - \lambda_k \Id)} $
		\item Osservo che questi sottospazi sono in somma diretta e quindi
		\begin{itemize}
			\item se $ V_{\lambda_1} \oplus \ldots \oplus V_{\lambda_k} = V $ allora $ T $ si diagonalizza e una base diagonalizzante di $ V $ è data dall'unione delle basi dei $ V_{\lambda_j} $ e posso procedere;
			\item se $ V_{\lambda_1} \oplus \ldots \oplus V_{\lambda_k} \subset V $ allora $ T $ non è diagonalizzabile.
		\end{itemize}
		Se voglio solo sapere se $ T $ è diagonalizzabile è sufficiente confrontare la molteplicità algebrica $ a_i $ delle radici $ \lambda_i $ del polinomio caratteristico con la molteplicità geometrica $ m_i = \dim{V_{\lambda_i}} = \dim{\ker{(T - \lambda_i \Id)} }$. $ T $ è diagonalizzabile $ \iff $ per ogni $ i $ vale $ a_i = m_i $.
		\item Usando la base trovata, si scrive la matrice diagonale corrispondente (i coefficienti sono zeri tranne sulla diagonale in cui ci sono tanti $ \lambda_i $ quanti la $ m_i = \dim{V_{\lambda_i}} $).
	\end{enumerate}
\end{framed}